function playtime_chart () {
    $(document).ready(function () {
        showGraph();
    });


    function showGraph()
    {
        {
            $.post("playtimechart.php",
            function (data)
            {
                console.log(data);
                var Date = [];
                var TPT = [];
                var FinAv = [];

                for (var i in data) {
                    Date.push(data[i].Date);
                    TPT.push(data[i].TPT);
                    FinAv.push(data[i].FinAv);
                }

                var chartdata = {
                    labels: Date,
                    datasets: [
                        {
                            label: 'Daily Playing Time',
                            backgroundColor: '#49e2ff',
                            borderColor: '#12CBE1',
                            hoverBackgroundColor: '#CCCCCC',
                            hoverBorderColor: '#666666',
                            data: TPT
                        },
                        {
                            label: 'Average Playing Time',
                            backgroundColor: '#49e2ff',
                            borderColor: '#922C4E',
                            hoverBackgroundColor: '#CCCCCC',
                            hoverBorderColor: '#666666',
                            data: FinAv
                        }

                    ]
                };

                var graphTarget = $("#graphCanvas");

                var barGraph = new Chart(graphTarget, {
                    type: 'line',
                    data: chartdata
                });
            });
        }
    }


}

function gamechart() {
$(document).ready(function () {
    showGraph();
});


function showGraph()
{
    {
        $.post("gamechart.php",
        function (data)
        {
            console.log(data);
            var MDate = [];
            var TP = [];
            var ExAv = [];

            for (var i in data) {
                MDate.push(data[i].MDate);
                TP.push(data[i].TP);
                ExAv.push(data[i].ExAv);
            }

            var chartdata = {
                labels: MDate,
                datasets: [
                    {
                        label: 'Monthly Spend',
                        backgroundColor: '#49e2ff',
                        borderColor: '#12CBE1',
                        hoverBackgroundColor: '#CCCCCC',
                        hoverBorderColor: '#666666',
                        data: TP
                    },
                    {
                        label: 'Average Monthly Spend past Year',
                        backgroundColor: '#49e2ff',
                        borderColor: '#922C4E',
                        hoverBackgroundColor: '#CCCCCC',
                        hoverBorderColor: '#666666',
                        data: ExAv
                    }

                ]
            };

            var graphTarget = $("#graphCanvas");

            var barGraph = new Chart(graphTarget, {
                type: 'line',
                data: chartdata,
                options: {
                    layout: {
                        padding: {
                            left: 50,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }
                    }
                }
            });
        });
    }
}
}

function titleschart() {
    $(document).ready(function () {
        showGraph();
    });
    
    
    function showGraph()
    {
        {
            $.post("titleschart.php",
            function (data)
            {
                console.log(data);
                var MDate = [];
                var Titles = [];
                    
                for (var i in data) {
                    MDate.push(data[i].MDate);
                    Titles.push(data[i].Titles);
                    
                }
    
                var chartdata = {
                    labels: MDate,
                    datasets: [
                        {
                            label: 'Monthly Games',
                            backgroundColor: '#49e2ff',
                            borderColor: '#12CBE1',
                            hoverBackgroundColor: '#CCCCCC',
                            hoverBorderColor: '#666666',
                            data: Titles
                        }
    
                    ]
                };
    
                var graphTarget = $("#graphCanvas2");
    
                var barGraph = new Chart(graphTarget, {
                    type: 'bar',
                    data: chartdata
                });
            });
        }
    }
    }