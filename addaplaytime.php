<!DOCTYPE html>
<html>
<?php
include('sessionredirect.php');
?>

<head>
  <link rel="stylesheet" href="styles.css">
</head>

<body>
  <header><a href="gametracker.php">Steam Sessions</a></header>
  <section>
  <?php include('nav.php');?>

    <h2>This is the page to add a game session</h2>
    <form action="" name="SavedGames" method="post">
      Game: <select name="GameSess">
        <?php
        include('dbfunctions.php');
        get_games_select('Game');
        
        if ($_POST) {
        save_gamingplaytime(); 
        }
        ?>
      </select><br><br>
      <label for="TotalHours">Total Hours:</label>
      <input id="TotalHours" name="TotalHours"><br>
      <label for="PlaytimeDate">Session Date:</label>
      <input type="date" id="PlaytimeDate" name="PlaytimeDate"><br>
      <input type="submit" value="save"><br>
    </form>
  </section>
</body>

</html>