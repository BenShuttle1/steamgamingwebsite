<?php
include('sessionredirect.php');

?>
<!DOCTYPE html>
<html>

<body>

  <head>
    <link rel="stylesheet" href="styles.css">
  </head>

  <body>


    <header><a href="gametracker.php">Steam Sessions</a></header>
    <section>
    <?php include('nav.php');?>
      
      <h2>This is the page to edit a session</h2>
      <?php
      include('dbfunctions.php');
      $SessID = $_GET['SessID'];
      get_gaming_session($SessID);

      if ($_POST) {
        playtime_edit();
        if(playtime_edit()) {
          update_playtimes();
        }
      }
      ?>
      <form action= "" name='EditPlaytime' method='post'>
      Game: <select name="GameSess">
        <?php
        get_games_select('Game');
        ?>
      </select><br><br>
      <label for="TotalHours">Total Hours:</label>
      <input id="TotalHours" name="TotalHours"><br>
      <label for="PlaytimeDate">Session Date:</label>
      <input type="date" id="PlaytimeDate" name="PlaytimeDate"><br>
      <input type="submit" value="save"><br>
      </form>
    </section>
  </body>

</html>