<?php
$GLOBALS ['$db'] ="db_games";
function get_games_select($order)
{   
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    if (!empty ($_SESSION['DaysDisplay']))
      { $Days = $_SESSION['DaysDisplay'];
  } else {
      $Days = 10000;
  }
    $sql = mysqli_query($con, "SELECT Game 
    FROM tbl_games where user = $UserID
    and DatePurchased > curdate() - interval $Days month  order by $order");
    while ($row = $sql->fetch_assoc()) {
        echo "<option value='" . $row['Game'] . "'>" . $row['Game'] . "</option>";
      };
}

function get_games_selectdlc()
{   
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    $sql = mysqli_query($con, "SELECT Game 
    FROM tbl_games where user = $UserID
    order by Game");
    while ($row = $sql->fetch_assoc()) {
        echo "<option value='" . $row['Game'] . "'>" . $row['Game'] . "</option>";
      };
}

function get_games_list($order)
{
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    if (!empty ($_SESSION['DaysDisplay']))
      { $Days = $_SESSION['DaysDisplay'];
  } else {
      $Days = 10000;
  }
    $sql = mysqli_query($con, 
        "SELECT ROW_NUMBER() OVER(ORDER BY $order asc) AS Rank, tg.Game, Price, DatePurchased, tsl.LTH as THD, round(Price/ tsl.LTH, 1) as Value
            FROM tbl_games tg
        
        left join

            (select ts.game, sum(ts.PlaytimeLength) as LTH
            from tbl_sessions ts
            where User = $UserID
            group by game) tsl 
            
        on tsl.game=tg.game

        where user = $UserID
        and tg.DatePurchased > curdate() - interval $Days month  
        ");

    
if ($sql) {
    echo "<table>
  <tr>
  <th style='width:15%'>Rank</th>
  <th style='width:50%'>Game</th>
  <th style='width:20%'>Price</th>
  <th style='width:30%'>Date Purchased</th>
  <th style='width:25%'>Total Time</th>
  <th style='width:20%'>Value (Won/Hr)</th>
  </tr>";

    while ($row = $sql->fetch_assoc()) {

      echo "<tr><td>" . $row["Rank"] . "</td><td>" . $row["Game"] . "</td><td>" . $row["Price"] . "</td><td>" . $row["DatePurchased"] . "</td><td>" . $row["THD"] . "</td><td>" . $row["Value"] . "</td></tr>";
    }
    echo "</table>";
  };
}

function get_games_list_value()
{
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    if (!empty ($_SESSION['DaysDisplay']))
      { $Days = $_SESSION['DaysDisplay'];
  } else {
      $Days = 10000;
  }
    $sql = mysqli_query($con,

        "select Current.Rank, Current.Game, Current.Price, Current.DatePurchased, Current.THD, Current.Value, Prev90.Rank-Current.Rank as Change2, ts3.Last as Last 
        from   
            (SELECT ROW_NUMBER() OVER(order by `Value` asc) as Rank, tg.Game, format(Price,'#,#') as Price, DatePurchased, tsl.LTH as THD, case when round(Price/tsl.LTH,1) is null then Price else round(Price/tsl.LTH,1) end as Value
            FROM tbl_games tg
            left join
                (select ts.game, sum(ts.PlaytimeLength) as LTH
                from tbl_sessions ts
                where User = $UserID
                group by game
                ) 
            tsl on tsl.game=tg.game
            where user = $UserID      
            ) 
        Current
        join 
            (SELECT ROW_NUMBER() OVER(order by `Value` asc) as Rank, tg.Game, format(Price,'#,#') as Price, DatePurchased, tsl.LTH as THD, case when round(Price/tsl.LTH,1) is null then Price else round(Price/tsl.LTH,1) end as Value
            FROM tbl_games tg
            left join
                (select ts.game, sum(ts.PlaytimeLength) as LTH
                from tbl_sessions ts
                where Playtimedate < curdate() - interval 365 day
                and User = $UserID
                group by game
                )
            tsl on tsl.game=tg.game
            where user = $UserID
            )
        Prev90   
        on Prev90.Game=Current.Game
        left join
            (select ts2.Game, max(ts2.PlaytimeDate) as Last
            from tbl_sessions ts2
            where user = $UserID
            and ts2.PlaytimeLength > 0
            and ts2.PlaytimeDate > '2021-09-01'
            group by ts2.Game
            )
        ts3 on ts3.Game=Current.Game
        where Current.DatePurchased > curdate() - interval $Days month
    "
    );

    if ($sql) {
        echo "<table>
      <tr>
        <th style='width:15%'>Rank</th>
        <th style='width:50%'>Game</th>
        <th style='width:20%'>Price</th>
        <th style='width:30%'>Date Purchased</th>
        <th style='width:25%'>Total Time</th>
        <th style='width:20%'>Value (Won/Hr)</th>
        <th style='width:20%'>Change 365 day</th>
        <th style='width:20%'>Last</th>
      </tr>";
  
        while ($row = $sql->fetch_assoc()) {
  
          echo "<tr><td>" . $row["Rank"] . "</td><td>" . $row["Game"] . "</td><td>" . $row["Price"] . "</td><td>" . $row["DatePurchased"] . "</td><td>" . $row["THD"] . "</td><td>" . $row["Value"] . "</td><td>" . $row["Change2"] . "</td><td>" . $row["Last"] . "</td></tr>";
        }
        echo "</table>";
      };
}

function get_games_list_time()
{
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    if (!empty ($_SESSION['DaysDisplay']))
      { $Days = $_SESSION['DaysDisplay'];
  } else {
      $Days = 10000;
  }
    $sql = mysqli_query($con, "

select Current.Rank, Current.Game, Current.Price, Current.DatePurchased, Current.THD, Current.Value, Prev90.Rank-Current.Rank as Change2, ts3.Last as Last from

    (SELECT ROW_NUMBER() OVER(order by `THD` desc, tg.Game desc) as Rank, tg.Game, format(Price,'#,#') as Price, DatePurchased, tsl.LTH as THD, case when round(Price/tsl.LTH,1) is null then Price else round(Price/tsl.LTH,1) end as Value
    FROM tbl_games tg
    left join

        (select ts.game, sum(ts.PlaytimeLength) as LTH
        from tbl_sessions ts
        where user = $UserID
        group by ts.game
        ) 
    tsl on tsl.game=tg.game
    where user = $UserID
    ) 
Current

join 
    (SELECT ROW_NUMBER() OVER(order by `THD` desc, tg.Game desc) as Rank, tg.Game, format(Price,'#,#') as Price, DatePurchased, tsl.LTH as THD, case when round(Price/tsl.LTH,1) is null then Price else round(Price/tsl.LTH,1) end as Value
    FROM tbl_games tg
    left join

        (select ts.game, sum(ts.PlaytimeLength) as LTH
        from tbl_sessions ts
        where Playtimedate < curdate() - interval 365 day
        and User = $UserID
        group by game
        ) tsl on tsl.game=tg.game
    where user = $UserID
    ) Prev90 on Prev90.Game=Current.Game

left join

    (select ts2.Game, max(ts2.PlaytimeDate) as Last
    from tbl_sessions ts2
    where user = $UserID
    and ts2.PlaytimeLength > 0
    and ts2.PlaytimeDate > '2021-09-01'
    group by ts2.Game
    ) ts3 on ts3.Game=Current.Game


where Current.DatePurchased > curdate() - interval $Days month
order by Current.THD desc, Current.Game desc
");
if ($sql) {
    echo "<table>
  <tr>
  <th style='width:15%'>Rank</th>
  <th style='width:50%'>Game</th>
  <th style='width:20%'>Price</th>
  <th style='width:30%'>Date Purchased</th>
  <th style='width:25%'>Total Time</th>
  <th style='width:20%'>Value (Won/Hr)</th>
  <th style='width:20%'>Change</th>
  <th style='width:20%'>Last</th>
  </tr>";

    while ($row = $sql->fetch_assoc()) {

      echo "<tr><td>" . $row["Rank"] . "</td><td>" . $row["Game"] . "</td><td>" . $row["Price"] . "</td><td>" . $row["DatePurchased"] . "</td><td>" . $row["THD"] . "</td><td>" . $row["Value"] . "</td><td>" . $row["Change2"] . "</td><td>" . $row["Last"] . "</td></tr>";
    }
    echo "</table>";
  };
}

function get_gaming_session($SessID)
{
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $SessID = $_GET['SessID'];
    $sql = mysqli_query($con, "

select Game, TotalHours, PlaytimeDate from tbl_sessions where ID=$SessID
");
echo "<table>
<tr>
    <th style='width:50%'>Game</th>
    <th style='width:20%'>Total Hours(Hrs)</th>
    <th style='width:20%'>Session Date</th>
</tr>";

    while ($row = $sql->fetch_assoc()) {

      echo "<tr>
        <td>" . $row["Game"] . "</td>
        <td>" . $row["TotalHours"] . "</td>
        <td>" . $row["PlaytimeDate"] . "</td>
    </tr>";
    }
    echo "</table>";
}

function edit_gaming_session($SessID)
{
    $SessID = $_GET['SessID']; //get DB ID for the gamingsession
    $Game = $_POST['GameSess']; //get the game of the gamingsession from the form
    $TotalHours = $_POST['TotalHours']; //get the Total Hours of the gamingsession from the form
    $PlaytimeDate = $_POST['PlaytimeDate']; //get the date of the gamingsession from the form

    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']); //where and how to connect to the db
    $sql2 = "SELECT IFNULL(sum(PlaytimeLength),0) from `tbl_sessions` WHERE `Game` = '$Game' and ID < $SessID"; //find the last playtime for this game
    $rs2 = mysqli_query($con, $sql2);
    $row2 = $rs2->fetch_array()[0] ?? '';
    $sql = mysqli_query($con, "UPDATE tbl_sessions
    
        SET Game = '$Game',
        TotalHours = '$TotalHours', 
        PlaytimeDate = '$PlaytimeDate', 
        PlaytimeLength = '$TotalHours'-'$row2',
        
        WHERE 
        ID = '$SessID'
    
    "); //update the row in the db with the new information

    return $sql;
}

function save_user()
{

    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);

    // get the post records
    $User = $_POST['user'];
    $Password = $_POST['pass'];



    // database insert SQL code
    $sql = "INSERT INTO `users`
	(`ID`, `Username`, `Password`)
	SELECT MAX(`ID`) +1, '$User', '$Password' from `users` ";

    // insert in database
    $rs = mysqli_query($con, $sql);

    if ($rs) {
        $query = mysqli_query($con, "SELECT * FROM users WHERE userName='$User' and password='$Password'");
        $row = mysqli_fetch_array($query);
        $num_row = mysqli_num_rows($query);

        if ($num_row > 0) {
            $_SESSION['ID'] = $row['ID']; {
                header('location: gametracker.php');
            }
        }
    }
}

function save_game () 
{$con = mysqli_connect('localhost', 'root', '',$GLOBALS['$db']);
$UserID = $_SESSION ['ID'];
// get the post records
$GName = $_POST['GName'];
$Price = $_POST['Price'];
$DatePurchased = $_POST['DatePurchased'];

$sqlcheck = mysqli_query($con, "SELECT Game from tbl_games where Game='$GName' and User = $UserID");

if(mysqli_num_rows ($sqlcheck) > 0) {

    echo "This game is already in your library";

    } else {

// database insert SQL code
            $sql = "INSERT INTO `tbl_games`
	        (`ID`, `Game`, `Price`, `DatePurchased`, `User`)
	        SELECT MAX(`ID`) +1, '$GName', '$Price', '$DatePurchased', '$UserID' from `tbl_games` ";

// insert in database
            $rs = mysqli_query($con, $sql);

            if($rs)
                {
                $sql2 = "INSERT INTO `tbl_sessions`
                (`ID`, `Game`, `TotalHours`, `PlaytimeDate`, `PlaytimeLength`,  `User`)
                SELECT MAX(`ID`) +1, '$GName', 0, '$DatePurchased', 0,  '$UserID' from `tbl_sessions` ";

// insert in database
                $rs2 = mysqli_query($con, $sql2);

                if($rs2)
	                {echo "<script>location.href='gamesort.php?sort=value';</script>"; 
	
                    }
                }
            }
}

function login_check()
{
    $con = mysqli_connect('localhost', 'root', '',$GLOBALS['$db']);

    if (isset($_POST['login']))
        {
            $username = mysqli_real_escape_string($con, $_POST['user']);
            $password = mysqli_real_escape_string($con, $_POST['pass']);

            $query = mysqli_query($con, "SELECT * FROM users WHERE userName='$username' and password='$password'");
            $row = mysqli_fetch_array($query);
            $num_row = mysqli_num_rows($query);

                if ($num_row > 0)
                      {
                          $_SESSION['ID']=$row['ID'];

                                {
                                    header('location: gametracker.php');
                                }
                        }
                    else
                        {
                            echo '<h2 style="color:black;">Invalid Username and or Password Combination</h2>';
                        }
        }


}

function save_gamingplaytime ()
{
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);

    // get the post records
    $Game = $_POST['GameSess'];
    $TotalHours = $_POST['TotalHours'];
    $PlaytimeDate = $_POST['PlaytimeDate'];
    $UserID = $_SESSION['ID'];
    // retrieve previous last Playtime
    $sql2 = "SELECT IFNULL(sum(`PlaytimeLength`),0) from `tbl_sessions` 
    WHERE `Game` = '$Game' and `User` = $UserID
    ";
    $rs2 = mysqli_query($con, $sql2);
    $row2 = $rs2->fetch_array()[0] ?? '';
    $UserID = $_SESSION['ID'];
    
    
    
    // database insert SQL code
    $sql = "INSERT INTO `tbl_sessions`
        (`ID`, `Game`, `TotalHours`, `PlaytimeDate`, `PlaytimeLength`,  `User`)
        SELECT MAX(`ID`) +1, '$Game', '$TotalHours', '$PlaytimeDate', '$TotalHours'  - '$row2',  $UserID from `tbl_sessions` ";
    
    // insert in database
    $rs = mysqli_query($con, $sql);
    
    if ($rs) {
        echo "<script>location.href='viewplaytimes.php?view=game';</script>";
    }


}

function playtime_delete ()
{
    $SessID = $_GET['SessID']; //get DB ID for the gamingsession

    $con = mysqli_connect('localhost', 'root', '',$GLOBALS['$db']); //where and how to connect to the db
    $sql = mysqli_query($con, "DELETE from tbl_sessions
    
        WHERE 
        ID = '$SessID'
    
    "); //delete the row in the db with the new information
    
    if($sql)  
        {
        echo "<script>location.href='viewplaytimes.php?view=game';</script>" ; //what to do if this request is successful
        }

}

function playtime_edit ()
{
    $SessID = $_GET['SessID']; //get DB ID for the gamingsession
    $Game = $_POST['GameSess']; //get the game of the gamingsession from the form
    $TotalHours = $_POST['TotalHours']; //get the Total Hours of the gamingsession from the form
    $PlaytimeDate = $_POST['PlaytimeDate']; //get the date of the gamingsession from the form
    
    $con = mysqli_connect('localhost', 'root', '',$GLOBALS['$db']); //where and how to connect to the db
    $sql2 = "SELECT IFNULL(sum(`PlaytimeLength`),0) from `tbl_sessions` WHERE `Game` = '$Game'"; //find the last playtime for this game
    $rs2 = mysqli_query($con, $sql2);
    $row2= $rs2->fetch_array()[0] ?? '';
    $sql = mysqli_query($con, "UPDATE tbl_sessions
    
        SET Game = '$Game',
        TotalHours = '$TotalHours', 
        PlaytimeDate = '$PlaytimeDate', 
        PlaytimeLength = '$TotalHours'-'$row2'
        
        WHERE 
        ID = '$SessID'
    
    "); //update the row in the db with the new information
    
    if($sql)  
        {
        echo "<script>location.href='viewplaytimes.php?view=game';</script>" ; //what to do if this request is successful
        }

}

function update_playtimes ()

{
$Game = $_POST['GameSess'];
$con = mysqli_connect('localhost', 'root', '',$GLOBALS['$db']);
$sql = mysqli_query($con, "UPDATE tbl_sessions

    join

        (select ID as UpdateID, TotalHours - coalesce(lag(TotalHours) over (order by id), 0) as NewPlaytimeLength
    
        from tbl_sessions

        where game = '$Game') newvalue
        
    on newvalue.UpdateID=tbl_sessions.ID 

set tbl_sessions.PlaytimeLength = newvalue.NewPlaytimeLength");

if($sql)  
        {
        echo "<script>location.href='viewplaytimes.php?view=game';</script>" ; //what to do if this request is successful
        }

}

function total_recent_playtime90 ()

{

$con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    $sql = "SELECT sum(PlaytimeLength) as `Tot` FROM tbl_sessions where PlaytimeDate > curdate() - interval 90 day and User = $UserID";
    $rs = mysqli_query($con, $sql);
    $row = $rs->fetch_assoc();
    $days = 90;
    echo "(" . $row["Tot"] . " hours, " . round($row["Tot"] / $days, 2) . " hours/day) ";

}

function total_recent_playtime60 ()

{

$con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    $sql = "SELECT sum(PlaytimeLength) as `Tot` FROM tbl_sessions where PlaytimeDate > curdate() - interval 60 day and User = $UserID";
    $rs = mysqli_query($con, $sql);
    $row = $rs->fetch_assoc();
    $days = 60;
    echo "(" . $row["Tot"] . " hours, " . round($row["Tot"] / $days, 2) . " hours/day) ";

}

function total_recent_playtime30 ()

{

$con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    $sql = "SELECT sum(PlaytimeLength) as `Tot` FROM tbl_sessions where PlaytimeDate > curdate() - interval 30 day and User = $UserID";
    $rs = mysqli_query($con, $sql);
    $row = $rs->fetch_assoc();
    $days = 30;
    echo "(" . $row["Tot"] . " hours, " . round($row["Tot"] / $days, 2) . " hours/day) ";

}

function display_recent_playtimes ()

{
$con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
      $UserID = $_SESSION['ID'];
      
      if (!empty ($_SESSION['DaysDisplay']))
      { $Days = $_SESSION['DaysDisplay'];
  } else {
      $Days = 180;
  }
      $sql = "SELECT ID as SessID, Game, PlaytimeLength, PlaytimeDate FROM tbl_sessions where PlaytimeDate > curdate()-interval $Days Day and User = $UserID order by PlaytimeDate desc";
      $rs = mysqli_query($con, $sql);

      if ($rs) {
        echo "<table>
    <tr>
      <th style='width:50%'>Game</th>
      <th style='width:20%'>Session Length(Hrs)</th>
      <th style='width:20%'>Session Date</th>
      <th style='width:10%'>Edit</th>
      <th style='width:10%'>X</th>
    </tr>";

        while ($row = $rs->fetch_assoc()) {

          echo "<tr>
      <td>" . $row["Game"] . "</td>
      <td>" . $row["PlaytimeLength"] . "</td>
      <td>" . $row["PlaytimeDate"] . "</td>
      <td><a href='editgamingplaytime.php?SessID=" . $row["SessID"] . "'><img src='assets/edit.png' style = 'width:50%'></a></td>
      <td><a href='deletegamingplaytime.php?SessID=" . $row["SessID"] . "'><img src='assets/delete.png' style = 'width:50%'></a></td>
    </tr>";
        }
        echo "</table>";
      }
}

function display_recent_playtitles ()

{
$con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
      $UserID = $_SESSION['ID'];
      
      if (!empty ($_SESSION['DaysDisplay']))
      { $Days = $_SESSION['DaysDisplay'];
  } else {
      $Days = 180;
  }
      $sql = "SELECT ID as SessID, Game, sum(PlaytimeLength) as PL, max(PlaytimeDate) as LPD FROM tbl_sessions where PlaytimeDate > curdate()-interval $Days Day and User = $UserID and PlaytimeLength > 0 group by Game order by LPD desc";
      $rs = mysqli_query($con, $sql);

      if ($rs) {
        echo "<table>
    <tr>
      <th style='width:50%'>Game</th>
      <th style='width:20%'>Total Session Length(Hrs)</th>
      <th style='width:20%'>Last Session(Hrs)</th>
      </tr>";

        while ($row = $rs->fetch_assoc()) {

          echo "<tr>
      <td>" . $row["Game"] . "</td>
      <td>" . $row["PL"] . "</td>
      <td>" . $row["LPD"] . "</td>
      </tr>";
        }
        echo "</table>";
      }
}

function logout()

{

// remove all session variables
session_unset();

// destroy the session
session_destroy();

//redirect
echo "<script>location.href='login.php';</script>";

}

function display_recent_playdays ()

{
$con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
      $UserID = $_SESSION['ID'];
      if (!empty ($_SESSION['DaysDisplay']))
      { $Days = $_SESSION['DaysDisplay'];
  } else {
      $Days = 365;
  }
      $sql = "SELECT tc.Date as Date, case when sum(ts.PL) is null then 0 else sum(ts.PL) end as TPT

      from tbl_calendar tc
      
      left join 
      
      (select PlaytimeDate PD, PlaytimeLength PL 
      
      from tbl_sessions 
      
      where user = $UserID) ts on ts.PD=tc.Date
      
      where tc.Date > curdate() - interval $Days day 
      and tc.Date <= curdate()
      
      group by tc.Date
      
      order by tc.Date desc;";

      $rs = mysqli_query($con, $sql);

      if ($rs) {
        echo "<table>
    <tr>
      <th style='width:20%'>Date</th>
      <th style='width:20%'>Total Playtime(Hrs)</th>
      
    </tr>";

        while ($row = $rs->fetch_assoc()) {

          echo "<tr>
      <td>" . $row["Date"] . "</td>
      <td>" . $row["TPT"] . "</td>
      
    </tr>";
        }
        echo "</table>";
      }
}

function add_dlc () {
    $con = mysqli_connect('localhost', 'root', '',$GLOBALS['$db']);
    $UserID = $_SESSION ['ID'];
    // get the post records
    $GName = $_POST['GName'];
    $Price = $_POST['Price'];
    
    $sql2 = "SELECT ID from tbl_games where Game = '$GName' and User = '$UserID'";
    $rs2 = mysqli_query($con, $sql2);

    $sql3 = "SELECT Price from tbl_games where Game = '$GName'";
    $rs3 = mysqli_query($con, $sql3);

    $row2= $rs2->fetch_array()[0] ?? '';
    $row3= $rs3->fetch_array()[0] ?? '';
    
    $sql = "UPDATE tbl_games
    
        SET Price = '$Price' + '$row3'
        
        WHERE 
        ID = '$row2'";

        $rs = mysqli_query($con, $sql);

        if($rs)
            {
	        
                echo "<script>location.href='gamesort.php?sort=value';</script>"; 
	
            }
        }

function playtimechart () {
    
    header('Content-Type: application/json');
    if (!empty ($_SESSION['DaysDisplay']))
            { $Days = $_SESSION['DaysDisplay'];
        } else {
            $Days = 180;
        }
    
    $UserID = $_SESSION['ID'];
    
    $sqlQuery = "
  select Table3.Date, Table3.TPT, Table3.FinAv

  from (
   
    select Date, Table2.TPT, avg(Table2.TPT) over (order by date ROWS between 13 preceding and current row) FinAv

    from
    
    (SELECT tc.Date as date, case when sum(ts.PL) is null then 0 else sum(ts.PL) end as TPT
    
    from tbl_calendar tc
    
    left join 
    
    (select PlaytimeDate PD, PlaytimeLength PL 
    
    from tbl_sessions 
    
    where User = $UserID) ts on ts.PD=tc.Date
    
    where tc.Date > curdate() - interval ($Days + 15) day
    and tc.Date <= curdate()
    
    group by tc.Date
    
    order by tc.Date asc) Table2) Table3

    where Table3.Date > curdate() - interval $Days day 
    
    limit 180


    ;";
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $result = mysqli_query($con,$sqlQuery);
    
    $data = array();
    foreach ($result as $row) {
        $data[] = $row;
    }
    
    mysqli_close($con);

    echo json_encode($data);




}

function gamechartspend () {
    $UserID = $_SESSION['ID'];
    if (!empty ($_SESSION['DaysDisplay']))
    { $Days = $_SESSION['DaysDisplay'];
} else {
    $Days = 10000;
}
    header('Content-Type: application/json');

    
    $sqlQuery = "
    select Table3.MDate, Table3.TP, Table3.ExAv

    from (
     
        select MDate, Table2.TP, avg(Table2.TP) over (order by MDate ROWS between 11 preceding and current row) ExAv
        from
        (
            SELECT date_format(tc.Date, '%Y-%m') as MDate, case when sum(tg.P) is null then 0 else sum(tg.P) end as TP
            from tbl_calendar tc
            left join 
                (select Price P, DatePurchased DP
                from tbl_games 
                where User = $UserID
                ) tg on tg.DP = tc.Date
            where tc.Date < curdate() + interval 1 day
            and tc.Date > curdate() - interval $Days month
            group by MDate
        ) Table2
    ) Table3


    ;";
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $result = mysqli_query($con,$sqlQuery);
    
    $data = array();
    foreach ($result as $row) {
        $data[] = $row;
    }
    
    mysqli_close($con);
    
    echo json_encode($data);




}

function total_spend () {

    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    $sql = "SELECT format(sum(Price), '#,#') as `Tot` FROM tbl_games where User = $UserID";
    $rs = mysqli_query($con, $sql);
    $row = $rs->fetch_assoc();
    echo "(" . $row["Tot"] . " KRW Total)</h2>";

}

function total_time () {

    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $UserID = $_SESSION['ID'];
    $sql = "
        select format(
            sum(
                ts.PlaytimeLength
                )
                , 'N' 
            )
        as TotalH 
        from tbl_sessions ts
        where User = $UserID"
    
    
    ;
    $rs = mysqli_query($con, $sql);
    $row = $rs->fetch_assoc();
    echo "(" . $row["TotalH"] . " Hours Total)</h2>";

}

function gamechartnumber () {
    $UserID = $_SESSION['ID'];
    header('Content-Type: application/json');
    if (!empty ($_SESSION['DaysDisplay']))
            { $Days = $_SESSION['DaysDisplay'];
        } else {
            $Days = 10000;
        }
    $sqlQuery = "
    select tc.MDate, case when tg.T is null then 0 else tg.T end as Titles

from

(select date_format(Date, '%Y-%m') as `MDate`

from tbl_calendar
where Date < curdate() + interval 1 day
and Date > curdate() - interval $Days month
group by MDate) tc

left join

(SELECT count(game) as T, date_format(DatePurchased, '%Y-%m') as DP

from tbl_games
where User = $UserID
group by DP) tg on tg.DP=tc.MDate



    
    ";
    $con = mysqli_connect('localhost', 'root', '', $GLOBALS['$db']);
    $result = mysqli_query($con,$sqlQuery);
    
    $data = array();
    foreach ($result as $row) {
        $data[] = $row;
    }
    
    mysqli_close($con);
    
    echo json_encode($data);




}