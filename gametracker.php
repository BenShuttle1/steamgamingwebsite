<!DOCTYPE html>
<html>
<?php
include('sessionredirect.php');
include('dbfunctions.php');
if (!empty($_GET['action'])) {

  $action = $_GET['action'];
    
    if($action == 'logout') {

    logout();
    
    }
  }
?>

<head>
  <link rel="stylesheet" href="styles.css">
</head>

<body>
  <header>
    Steam Sessions</header>
  <section>
  <?php include('nav.php');?>

    <h2>This is a site to keep track of your Steam games</h2>

  </section>
  <div class="popup" onclick="myFunction()">Click me!
  <span class="popuptext" id="myPopup">Popup text...</span>
</div>
<script>
// When the user clicks on <div>, open the popup
function myFunction() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}
</script>
</body>

</html>